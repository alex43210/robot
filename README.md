Robot
-----
Python library (depends from my text classifier - [pynlc](https://bitbucket.org/alex43210/pynlc)) that allows to make nlc-based robots.
Also can use some simple entity extractuion.

Example bot script:
```
script:
  text: Hello. I'm weather robot. How can I help you?
  name: main
  children:
    -
      name: city_conditions
      text: "Conditions in {%print $city%} is next."
      conditions:
        class: conditions
        entity_variables:
          city: $city
    -
      name: city_temperature
      text: "Temperature in {%print $city%} is next."
      conditions:
        class: temperature
        entity_variables:
          city: $city
    -
      name: conditions
      text: Okay, type city name.
      conditions:
        class: conditions
      children:
        -
          name: conditions_city_selected
          text: "{%goto city_conditions%}"
          conditions:
            entity_variables:
              city: $city
    -
      name: temperature
      text: Okay, type city name.
      conditions:
        class: temperature
      children:
        -
          name: temperature_city_selected
          text: "{%goto city_temperature%}"
          conditions:
            entity_variables:
              city: $city
entities:
  city:
    - Moscow
    - Tokyo
    - Ottava
synonyms:
  Moscow:
    - default city
    - DC
classes:
  temperature:
    - How hot is it today?
    - Is it hot outside?
    - Will it be uncomfortably hot?
    - Will it be sweltering?
    - How cold is it today?
    - Is it cold outside?
    - Will it be uncomfortably cold?
    - Will it be frigid?
    - What is the expected high for today?
    - What is the expected temperature?
    - Will high temperatures be dangerous?
    - Is it dangerously cold?
    - When will the heat subside?
    - Is it hot?
    - Is it cold?
    - How cold is it now?
    - Will we have a cold day today?
    - When will the cold subside?
    - What highs are we expecting?
    - What lows are we expecting?
    - Is it warm?
    - Is it chilly?
    - What's the current temp in Celsius?
    - What is the temperature in Fahrenheit?
  conditions:
    - Is it windy?
    - Will it rain today?
    - What are the chances for rain?
    - Will we get snow?
    - Are we expecting sunny conditions?
    - Is it overcast?
    - Will it be cloudy?
    - How much rain will fall today?
    - How much snow are we expecting?
    - Is it windy outside?
    - How much snow do we expect?
    - Is the forecast calling for snow today?
    - Will we see some sun?
    - When will the rain subside?
    - Is it cloudy?
    - Is it sunny now?
    - Will it rain?
    - Will we have much snow?
    - Are the winds dangerous?
    - What is the expected snowfall today?
    - Will it be dry?
    - Will it be breezy?
    - Will it be humid?
    - What is today's expected humidity?
    - Will the blizzard hit us?
    - Is it drizzling?
classifier_params:
  filter_sizes: [1]
  hidden_size: 100
  nb_filter: 150
min_class_confidence: 0.8
```
and test code :
```
script = Script.load(os.path.join(os.path.dirname(__file__), "script.yaml"))
text_processor = TextProcessor("english",
                               [["turn", "on"], ["turn", "off"]],
                               Word2Vec.load_word2vec_format(
                                    os.path.join(os.path.dirname(__file__), "glove.6B.100d.txt")
                               ))
robot = Robot(script, text_processor)
state, output = robot.output()
self.assertEqual(output, "Hello. I'm weather robot. How can I help you?")
self.assertIsNotNone(state)
self.assertEqual(state.stage, "main")
self.assertEqual(state.variables, {})
state, output = robot.answer(state, "How cold it'll be in Moscow today?")
self.assertIsNotNone(state)
self.assertEqual(state.stage, "city_temperature")
self.assertEqual(state.variables, {"$city": ["Moscow"]})
self.assertEqual(output, "Temperature in Moscow is next.")
```
