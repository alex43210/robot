from unittest import TestCase
import os
from .script import Script


class ScriptTest(TestCase):
    def testLoading(self):
        path = os.path.join(os.path.dirname(__file__), "script.yaml")
        script = Script.load(path)
        text = "temperature How cold it'll be in Moscow and tokyo?"
        self.assertEqual(script.contained_entities(text), {'city': ['Moscow', 'Tokyo']})
        self.assertTrue(script.contains_class("temperature", text))
        self.assertFalse(script.contains_class("conditions", text))
        self.assertEqual(
            script.script_items['city_temperature'].validate_conditions(script, {}, text),
            (True, {'$city': ['Moscow', 'Tokyo']})
        )
