from pynlc import TextProcessor
from unittest import TestCase
from .robot import Robot
from .script import Script
from gensim.models import Word2Vec
import os


class RobotTest(TestCase):
    def testRobot(self):
        script = Script.load(os.path.join(os.path.dirname(__file__), "script.yaml"))
        text_processor = TextProcessor("english",
                                       [["turn", "on"], ["turn", "off"]],
                                       Word2Vec.load_word2vec_format(
                                           os.path.join(os.path.dirname(__file__), "glove.6B.100d.txt")
                                       ))
        robot = Robot(script, text_processor)
        state, output = robot.output()
        self.assertEqual(output, "Hello. I'm weather robot. How can I help you?")
        self.assertIsNotNone(state)
        self.assertEqual(state.stage, "main")
        self.assertEqual(state.variables, {})
        state, output = robot.answer(state, "How cold it'll be in Moscow today?")
        self.assertIsNotNone(state)
        self.assertEqual(state.stage, "city_temperature")
        self.assertEqual(state.variables, {"$city": ["Moscow"]})
        self.assertEqual(output, "Temperature in Moscow is next.")
