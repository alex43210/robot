import itertools
import copy


def _variable_string_presentation(variable):
    assert isinstance(variable, str) or \
        isinstance(variable, list)
    if isinstance(variable, str):
        return variable
    else:
        return " ".join([_variable_string_presentation(var) for var in variable])


def _print(stage, state, args):
    return stage, " ".join([_variable_string_presentation(state[name])
                            for name in args])


def _goto(stage, state, args):
    return args[0], ""


def _empty(stage, state, args):
    return stage, ""


def process_output(text, stage, state, handlers):
    """
    Process output
    :param text: text pattern
    :type text: str
    :param stage: current stage
    :type stage: str
    :param state: current state
    :type state: dict[str, str]
    :param handlers: handlers. \
      Handler get current stage, state, and arguments and return new stage (can be same) and output
    :type handlers: dict[str, (str, dict[str, str], list[str])->(str, str)]
    :return: new stage (can be same) and output
    :rtype: (str, str)
    """
    handlers = copy.copy(handlers)
    std_handlers = {
        "print": _print,
        "goto": _goto,
        "": _empty
    }
    for name, handler in std_handlers.items():
        handlers[name] = handler
    parts = (" " + text).split("{%")
    splitted_parts = list(itertools.chain(*[part.split("%}") for part in parts]))
    while len(splitted_parts) % 3 != 0:
        splitted_parts.append("")
    grouped_parts = []
    for i in range(0, int(len(splitted_parts) / 3)):
        grouped_parts.append([
            splitted_parts[i * 3],
            splitted_parts[i * 3 + 1],
            splitted_parts[i * 3 + 2],
        ])
    text = []
    for part in grouped_parts:
        text.append(part[0])
        command = part[1]
        command_parts = command.split(" ")
        handler = handlers[command_parts[0]]
        stage, output = handler(stage, state, command_parts[1:])
        text.append(output)
        text.append(part[2])
    return stage, "".join(text).strip()
