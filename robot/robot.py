from pynlc import TextClassifier, TextProcessor
from keras.callbacks import EarlyStopping
import numpy
from .script import Script
from .robot_state import RobotState
from .output_processing import process_output


class Robot:
    """
    Robot class.
    """

    def __init__(self, script, text_processor, output_handlers=None, config=None):
        """
        Initialize robot
        :param script: script
        :type script: Script
        :param text_processor: text processor
        :type text_processor: TextProcessor
        :param output_handlers: output handlers \
            Handler get current stage, state, and arguments \
              and return new stage (can be same) and output
        :type output_handlers: dict[str, (str, dict[str, str], list[str])->(str, str)]|NoneType
        :param config: configuration
        :type config: dict
        """
        self.script = script
        self.text_processor = text_processor
        if output_handlers is not None:
            self.output_handlers = output_handlers
        else:
            self.output_handlers = {}
        self.config = config
        self.classifier, self.config = self._build_classifier_config(config)

    def _build_classifier_config(self, config):
        """
        Build classifier
        :rtype: (TextClassifier, dict)
        """
        if config is None:
            classifier = TextClassifier(self.text_processor, backend_extra_args={
                'filter_sizes': self.script.classifier_filter_sizes,
                'nb_filter': self.script.classifier_nb_filter,
                'hidden_size': self.script.classifier_hidden_size
            })
            texts, classes = self.script.classifier_train_data()
            classifier.train(texts, classes, 200, verbose=True, callbacks=[
                EarlyStopping('val_loss', 1e-3, 10, verbose=True)
            ])
            config = {
                'classifier': classifier.config
            }
            return classifier, config
        else:
            classifier = TextClassifier(self.text_processor, **config['classifier'])
            return classifier, self.config

    def output(self, state=None):
        """
        Get output.
        :param state: robot state
        :type state: RobotState
        :return: new state and output
        :rtype: (RobotState, str)
        """
        if state is None:
            state = RobotState()
        old_stage = state.stage
        while True:
            new_stage, output = process_output(self.script.script_items[old_stage].text,
                                               old_stage,
                                               state.variables, self.output_handlers)
            if new_stage == old_stage:
                break
            else:
                old_stage = new_stage
        return RobotState(new_stage, state.variables), output

    def answer(self, state, input_text):
        """
        Answer to input text
        :param state: robot state
        :type state: RobotState
        :param input_text: input text
        :type input_text: str
        :return: new state (or None) and answer text
        :rtype: (RobotState|NoneType, str)
        """
        prediction = self.classifier.predict([input_text])[0]
        classes = self.classifier.classes
        values = [prediction[class_name] for class_name in classes]
        top_class = classes[numpy.argmax(numpy.array(values))]
        top_confidence = prediction[top_class]
        if top_confidence >= self.script.min_class_confidence:
            class_name = top_class
        else:
            class_name = ""
        processed_input = "{0} {1}".format(class_name, input_text)
        new_state = state.switch(self.script, processed_input)
        if new_state is None:
            return None, ""
        else:
            return self.output(new_state)
