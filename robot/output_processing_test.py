from unittest import TestCase
from .output_processing import process_output


class OutputProcessingTest(TestCase):
    def testProcessing(self):
        self.assertEqual(
            process_output("Conditions in {%print city%} is next.", "city_conditons",
                           {"city": "Moscow"}, {}),
            ('city_conditons', 'Conditions in Moscow is next.',)
        )
        self.assertEqual(
            process_output("{%goto city%}", "city_conditons",
                           {"city": "Moscow"}, {}),
            ('city', '',)
        )
