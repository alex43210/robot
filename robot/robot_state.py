from .script import Script


class RobotState:
    """
    Robot state (script stage id, variables)
    """
    def __init__(self, stage="main", variables={}):
        """
        Initialize robot state
        :param stage: current stage
        :type stage: str
        :param variables: variables
        :type variables: dict[str, str]
        """
        self.stage = stage
        self.variables = variables

    def switch(self, script, text):
        """
        Check switches
        :param script: script
        :type script: Script
        :param text: processed text
        :type text: str
        :return: new state or None
        :rtype: RobotState|NoneType
        """
        current_item = script.script_items[self.stage]
        for child in current_item.children:
            valid, new_state = child.validate_conditions(script, self.variables, text)
            if valid:
                return RobotState(child.name, new_state)
        return None
