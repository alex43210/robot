import copy
from io import StringIO
import yaml


class ScriptItem:
    """
    One script tree item
    """
    def __init__(self, data):
        self.text = data['text']
        self.name = data['name']
        self.children = []
        if 'children' in data:
            for child in data['children']:
                self.children.append(ScriptItem(child))
        if 'conditions' in data:
            self.condition_class = data['conditions'].get('class', None)
            self.entity_variables = data['conditions'].get('entity_variables', {})
        else:
            self.condition_class = None
            self.entity_variables = {}

    def iterate(self, iteration):
        """
        Iterate script tree
        :param iteration: iteration function
        :type iteration: (ScriptItem)->NoneType
        """
        iteration(self)
        for child in self.children:
            child.iterate(iteration)

    def validate_conditions(self, script, state, text):
        """
        Check item conditions
        :param script: script
        :type script: Script
        :param state: current state variables
        :type state: dict[str, str]
        :param text: processed input
        :type text: str
        :return: valid flag and new state
        :rtype: (bool, dict[str, str])
        """
        if not script.contains_class(self.condition_class, text):
            return False, state
        contained_entities = script.contained_entities(text)
        entity_types = set(contained_entities.keys())
        needed_types = set(self.entity_variables.keys())
        if len(needed_types - entity_types) != 0:
            return False, state
        new_state = copy.deepcopy(state)
        for entity_type, variable in self.entity_variables.items():
            values = contained_entities[entity_type]
            new_state[variable] = values
        return True, new_state


class Script:
    def __init__(self, source):
        """
        Parse script
        :param source: source code
        :type source: str
        """
        data = yaml.load(StringIO(source))
        self.entities = self._build_entities(data)
        self.synonyms = self._build_synonyms(data)
        self.classes = data['classes']
        self.script = ScriptItem(data['script'])
        self.script_items = self._build_script_items(self.script)
        default_classifier_params = {'filter_sizes': [1, 2], 'hidden_size': 200, 'nb_filter': 150}
        classifier_params = data.get('classifier_params', default_classifier_params)
        self.classifier_filter_sizes = classifier_params.get(
            'filter_sizes',
            default_classifier_params['filter_sizes'])
        self.classifier_hidden_size = classifier_params.get(
            'hidden_size',
            default_classifier_params['hidden_size'])
        self.classifier_nb_filter = classifier_params.get(
            'nb_filter',
            default_classifier_params['nb_filter'])
        self.min_class_confidence = data.get('min_class_confidence', 0.8)

    def _build_script_items(self, script):
        items = {}

        def _iteration(item):
            items[item.name] = item

        script.iterate(_iteration)
        return items

    def _build_entities(self, data):
        """
        Build entities
        :rtype: dict[str, list[str]]
        """
        entities = {}
        for name, values in data['entities'].items():
            entities[name] = [value for value in values]
        return entities

    def _build_synonyms(self, data):
        """
        Build synonyms
        :rtype: dict[str, list[str]]
        """
        synonyms = {}
        for canonycal, others in data['synonyms'].items():
            synonyms[canonycal.lower] = [other.lower() for other in others]
        return synonyms

    @staticmethod
    def load(path):
        """
        Load script from file
        :param path: file path
        :type path: str
        :return: script
        :rtype: Script
        """
        with open(path, "r", encoding="utf-8") as file:
            return Script(file.read())

    def contained_entities(self, text):
        """
        Is text contains given entity?
        :param text: text
        :type text: str
        :return: contains flag
        :rtype: dict[str, list[str]]
        """
        text = text.lower()
        contained = {}
        for entity, entity_items in self.entities.items():
            entity_matches = []
            for entity_item in entity_items:
                if entity_item.lower() in text:
                    entity_matches.append(entity_item)
                if entity_item.lower() in self.synonyms:
                    for synonym in self.synonyms[entity_item]:
                        if synonym in text:
                            entity_matches.append(entity_item)
            if len(entity_matches) > 0:
                contained[entity] = entity_matches
        return contained

    def contains_class(self, class_name, text):
        """
        Is classified input matched given class
        :param class_name: class name (if not need validation)
        :type class_name: str|NoneType
        :param text: processed input text
        :type text: str
        :return: match flag
        :rtype: bool
        """
        if class_name is None:
            return True
        return text.startswith(class_name)

    def classifier_train_data(self):
        """
        Get data to train classifier
        :return: train records One class list for each text
        :rtype: (list[str], list[list[str]])
        """
        texts = []
        classes = []
        for class_name, class_texts in self.classes.items():
            for text in class_texts:
                texts.append(text)
                classes.append([class_name])
        return texts, classes
