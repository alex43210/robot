from unittest import TestCase
import os
from .script import Script
from .robot_state import RobotState


class RobotStateTest(TestCase):
    def testSwitching(self):
        path = os.path.join(os.path.dirname(__file__), "script.yaml")
        script = Script.load(path)
        text = "temperature How cold it'll be in Moscow and tokyo?"
        state = RobotState()
        new_state = state.switch(script, text)
        self.assertIsNotNone(new_state)
        self.assertEqual(new_state.stage, "city_temperature")
        self.assertEqual(new_state.variables, {"$city": ["Moscow", "Tokyo"]})

    def testFullSwitchingScript(self):
        test_script = [
            ("main", "conditions", "conditions It'll be rainy?", {}, {}),
            ("conditions", "conditions_city_selected", "Moscow", {}, {"$city": ["Moscow"]}),
            ("conditions", "conditions_city_selected", "Tokyo", {}, {"$city": ["Tokyo"]}),
            ("conditions", "conditions_city_selected", "Ottava", {}, {"$city": ["Ottava"]}),

            ("main", "temperature", "temperature It'll be cold?", {}, {}),
            ("temperature", "temperature_city_selected", "Moscow", {}, {"$city": ["Moscow"]}),
            ("temperature", "temperature_city_selected", "Tokyo", {}, {"$city": ["Tokyo"]}),
            ("temperature", "temperature_city_selected", "Ottava", {}, {"$city": ["Ottava"]}),

            ("main", "city_conditions", "conditions It'll be rainy in Moscow?", {}, {"$city": ["Moscow"]}),
            ("main", "city_conditions", "conditions It'll be rainy in Tokyo?", {}, {"$city": ["Tokyo"]}),
            ("main", "city_conditions", "conditions It'll be rainy in Ottava?", {}, {"$city": ["Ottava"]}),

            ("main", "city_temperature", "temperature It'll be cold in Moscow?", {}, {"$city": ["Moscow"]}),
            ("main", "city_temperature", "temperature It'll be cold in Tokyo?", {}, {"$city": ["Tokyo"]}),
            ("main", "city_temperature", "temperature It'll be cold in Ottava?", {}, {"$city": ["Ottava"]}),
        ]
        path = os.path.join(os.path.dirname(__file__), "script.yaml")
        script = Script.load(path)
        for record in test_script:
            source_stage, target_stage, input_text, source_state, target_state = record
            source_robot_state = RobotState(source_stage, source_state)
            new_robot_state = source_robot_state.switch(script, input_text)
            self.assertIsNotNone(new_robot_state)
            self.assertEqual(target_stage, new_robot_state.stage)
            self.assertEqual(target_state, new_robot_state.variables)

